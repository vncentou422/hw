//Vincent Ou
//pd 9
//HW#34
//2013-11-27

/*==================================================
  class BinSearch
  ==================================================*/

public class BinSearch2 {


    /*==================================================
      int binSearch(int[],int) -- searches an array of ints for target int
      pre:  input array is sorted in ascending order
      post: returns index of target, or returns -1 if target not found
      ==================================================*/
    public static int binSearch ( Comparable [] a, int target ) {
	//uncomment exactly 1 of the 2 stmts below:

	return binSearchIter( a, target, 0, a.length-1 );
	//return binSearchRec( a, target, 0, a.length-1 );
    }


    public static int binSearchRec( Comparable [] a, Comparable target, int lo, int hi ) {
	int x = (lo + hi)/2;
	if (lo > hi){
	    return -1;
	}
	else {
	    if (a[x].equals(target)){
		return x;
	    }
	    else if (a[x].compareTo(target) < 0){
		lo = x + 1;
		return binSearchRec( a, target, lo, hi);
	    }
	    else if (a[x].compareTo(target) > 0 ){
		hi = x + 1;
		return binSearchRec( a, target, lo, hi);
	    }
	    else
		return -1;
	}
    }


    public static int binSearchIter( Comparable[] a, Comparable target, int lo, int hi ) {   
	while (lo <= hi){
	    int y = (lo + hi)/2;
	    if (a[y].equals(target)){
		return y;
	    }
	    else if( a[y].compareTo(target) < 0){
		lo = y + 1;
	    }
	    else if (a[y].compareTo(target)> 0){
		hi = y - 1;
	    }
	}
	return -1;
    }//end binSearchIter



    //tell whether an array is sorted in ascending order
    private static boolean isSorted( Comparable[] arr ) {

	boolean retBoo = true; //init to true, assume array is sorted

	for( int i=0; i < arr.length-1; i++ ) {
	    if ( arr[i].compareTo(arr[i+1]) > 0 ) {
		return false;
	    }
	}
	return retBoo; //if entire array was traversed, it must be sorted
    }


    // utility/helper fxn to display contents of an array of Objects
    private static void printArray( Comparable [] arr ) {
	String output = "[ "; 

	for( Comparable i : arr )
	    output += i + ", ";

	output = output.substring( 0, output.length()-2 ) + " ]";

	System.out.println( output );
    }



    //main method for testing
    public static void main ( String[] args ) {

	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	System.out.println("\nNow testing binSearch on int array...");

	//Declare and initialize array of ints
	Comparable [] iArr = { 2, 4, 6, 8, 6, 42 };
	printArray( iArr );
	System.out.println( "iArr1 sorted? -- " + isSorted(iArr) );

	Comparable [] iArr2 = { 2, 4, 6, 8, 13, 42 };
	printArray( iArr2 );
	System.out.println( "iArr2 sorted? -- " + isSorted(iArr2) );

	//search for 6 in array 
	System.out.println( binSearch(iArr2,2) );
	System.out.println( binSearch(iArr2,4) );
	System.out.println( binSearch(iArr2,6) );
	System.out.println( binSearch(iArr2,8) );
	System.out.println( binSearch(iArr2,13) );
	System.out.println( binSearch(iArr2,42) );

	//search for 43 in array 
	System.out.println( binSearch(iArr2,43) );

    }//end main()

}//end class BinSearch
