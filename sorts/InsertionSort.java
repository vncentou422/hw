//Vincent Ou
//pd 9
//HW#46
//2014-1-2

/*======================================
  class InsertionSort -- implements insertionsort algorithm
  ======================================*/
/*
Q1: Is more work done toward beginning or end of insertion sort? Why?
More work is done towards the end because the function has to resort the sorted side of the list and as it nears the end, the list begins to grow bigger and bigger, thus more work is done
Q2: For n items, how many passes are necessary to sort?
n-1
Q3: What is known after pass p, and how do you know it?
It is known that all p+1 items are sorted on the left because the first element is already sorted, and for every increase in p, another item is sorted into the sorted side of the list.
Q4: What is the runtime classification of this sort?
O(n*n)
*/

import java.util.ArrayList;

public class InsertionSort {

    //precond: lo < hi && size > 0
    //postcond: returns an ArrayList of random integers
    //          from lo to hi, inclusive
    public static ArrayList populate( int size, int lo, int hi ) {
        ArrayList<Integer> retAL = new ArrayList<Integer>();

        for ( int x = 0; x < size; x++){
            retAL.add(((int)((hi + 1 - lo) * Math.random() - lo)));
        }

        return retAL;
    }


    //randomly rearrange elements of an ArrayList
    public static void shuffle( ArrayList al ) {
        for (int x = 0; x < al.size(); x++){
            int y = (int)(al.size() * Math.random());
            int z = (int)(al.size() * Math.random());
            al.set(y , al.set( z, al.get(y)));
        }

    }


    // VOID version of insertionSort
    // Rearranges elements of input ArrayList
    // postcondition: data's elements sorted in ascending order
    public static void insertionSortV( ArrayList<Comparable> data ) {
	int divider = 0;
        for (int count = 0; count < data.size() - 1; count++){
	    if (data.get(divider).compareTo(data.get(count)) >= 0){
		divider++;
		for (int county = 0; county < divider; county++){
		    for ( int x = divider; x > 0; x--){
			if(data.get(x).compareTo(data.get(x - 1)) < 0){
			    data.set(x - 1, data.set(x , data.get( x - 1)));
			}
		    }
		}
	    }
	    else
		divider++;
	}
    }//end insertionSortV -- O(?)


    // ArrayList-returning insertionSort
    // postcondition: order of data's elements unchanged
    //                Returns sorted copy of data.
    public static ArrayList<Comparable> insertionSort( ArrayList<Comparable> input ) {
        //declare and initialize empty ArrayList for copying
        ArrayList<Comparable> data = new ArrayList<Comparable>();
        for( int x = 0; x < input.size(); x++){
            data.add(input.get(x));
        }
        insertionSortV ( data);        
        return data;
    }//end insertionSort -- O(?)


    public static void main(String [] args){

        /*===============for VOID methods=============
============================================*/
          
        ArrayList glen = new ArrayList<Integer>();
        glen.add(7);
        glen.add(1);
        glen.add(5);
        glen.add(12);
        glen.add(3);
        System.out.println( "ArrayList glen before sorting:\n" + glen );
        insertionSortV(glen);
        System.out.println( "ArrayList glen after sorting:\n" + glen );

        ArrayList coco = populate( 10, 1, 1000 );
        System.out.println( "ArrayList coco before sorting:\n" + coco );
        insertionSortV(coco);
        System.out.println( "ArrayList coco after sorting:\n" + coco );
          

        /*==========for AL-returning methods==========
         
            ArrayList glen = new ArrayList<Integer>();
        glen.add(7);
        glen.add(1);
        glen.add(5);
        glen.add(12);
        glen.add(3);
        System.out.println( "ArrayList glen before sorting:\n" + glen );
        ArrayList glenSorted = insertionSort( glen );
        System.out.println( "sorted version of ArrayList glen:\n" 
                            + glenSorted );
        System.out.println( "ArrayList glen after sorting:\n" + glen );

              ArrayList coco = populate( 10, 1, 1000 );
        System.out.println( "ArrayList coco before sorting:\n" + coco );
        ArrayList cocoSorted = insertionSort( coco );
        System.out.println( "sorted version of ArrayList coco:\n" 
                            + cocoSorted );
        System.out.println( "ArrayList coco after sorting:\n" + coco );
        System.out.println( coco );
           ============================================*/

    }//end main

}//end class InsertionSort
