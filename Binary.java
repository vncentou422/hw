//Vincent Ou
//pd 9 
//HW#29
//2013-11-20

//skeleton file for class Binary
// eventually should adhere to specifications of Comparable interface

public class Binary implements Comparable{

    private int _decNum;
    private String _binNum;

    /*=====================================
      default constructor
      precondition:  
      postcondition: 
      =====================================*/
    public Binary() { 
	_decNum = 0;
	_binNum = "0";
    }


    /*=====================================
      overloaded constructor
      precondition:  n >= 0
      postcondition: 
      =====================================*/
    public Binary( int n ) {
	if (n >= 0){
	    _decNum = n;
	}
	else
	    _decNum = 0;
	_binNum = decToBin(_decNum);

    }

    /*===============================
     accessors
     ================*/
    public int getDecNum(){
	return _decNum;
    }
    public String getBinNum(){
	return _binNum;
    }
    /*=====================================
      String toString() -- returns String representation of this Object
      precondition:  
      postcondition: 
      =====================================*/
    public String toString() { 
	return _binNum;
    }


    /*=====================================
      String decToBin(int) -- converts base-10 input to binary
      precondition:  n >= 0
      postcondition: return String of bits
      eg  decToBin(14) -> "1110"
      =====================================*/
    public static String decToBin( int n ) {
        String retStr = "" + (n%2);
	while (n > 0){
	    retStr = "" + ((n/2) % 2) + retStr;
	    n /= 2;
	}
	return retStr.substring(1);
    }


    /*=====================================
      String decToBinR(int) -- converts base-10 input to binary, recursively
      precondition:  n >= 0
      postcondition: returns String of bits
      eg  decToBin(14) -> "1110"
      =====================================*/
    public static String decToBinR( int n ) { 
	if ((n > 0)){
	    return  decToBinR(n/2) + (n%2);
	}
	else
	    return "";
        
    }

     public boolean equals( Object other ) { 
	return this == other
	    ||
	    (other instanceof Binary
	     && this._decNum == ((Binary)other)._decNum);
    }
    public int compareTo( Object other ) {

	if ( ! (other instanceof Binary) )
	    throw new ClassCastException("\ncompareTo() input not Binary");

	return this._decNum - ((Binary)other)._decNum;
    }
    
    //main method for testing
    public static void main( String[] args ) {

	System.out.println();
	System.out.println( "Testing ..." );
        
	
	Binary b1 = new Binary(5);
	
	Binary b2 = new Binary(5);
	Binary b3 = b1;
	System.out.println(b1);
	System.out.println(b2);
	System.out.println(b3);       //false b/c b1, b2 not aliases
	System.out.println(b1 == b2); //true b/c b1, b3 are aliases
	System.out.println(b1 == b3); //false, but should be true
	System.out.println(b1.equals(b2));
	
    }//end main()

} //end class
