
//Vincent Ou
//pd 9
//HW#33
//2013-11-26

/*==================================================
  class BinSearch
  skeleton file
  ==================================================*/

public class BinSearch {


    /*==================================================
      int binSearch(int[],int) -- searches an array of ints for target int
      pre:  input array is sorted in ascending order
      post: returns index of target, or returns -1 if target not found
      ==================================================*/
    public static int binSearch ( int[] a, int target ) {
	//uncomment exactly 1 of the 2 stmts below:
	//return binSearchIter( a, target, 0, a.length-1 );
	return binSearchRec( a, target, 0, a.length-1 );
    }


    public static int binSearchRec( int[] a, int target, int lo, int hi ) {
	int x = (lo + hi)/2;
	if (lo == hi){
	    return -1;
	}
	else if (a[x] < target){
	    lo = x + 1;
	    return binSearchRec( a, target, lo, hi);
	}
	else if (a[x] > target){
	    hi = x + 1;
	    return binSearchRec( a, target, lo, hi);
	}
	else
	    return x;
    }
    public static int binSearchIter( int[] a, int target, int lo, int hi ) {

	int x = -1;
	while (!(lo == hi)){
	    int y = (lo + hi)/2;
	    if (a[y] == target){
		x = y;
		break;
	    }
	    else if( a[y] < target){
		lo = y + 1;
	    }
	    else if (a [y] > target){
		hi = y + 1;
	    }
	}
	return x;
		
    }



    //tell whether an array is sorted in ascending order
    private static boolean isSorted( int[] arr ) {
	boolean sort = true;
	for (int x = 1; x < arr.length; x++){
	    if (arr[x-1] > arr[x]){
		sort = false;
	    }
	}
	return sort;
    }
    /*
    private static boolean isSorted( Comparable arr ) {
	boolean sort = true;
	for (int x = 1; x < arr.length; x++){
	    if (arr[x-1].compareTo(arr[x]) > 0){
		sort = false;
	    }
	}
	return sort; 
    }
    */

    // utility/helper fxn to display contents of an array of Objects
    private static void printArray( int[] arr ) {
	String output = "[ "; 

	for( int i : arr )
	    output += i + ", ";

	output = output.substring( 0, output.length()-2 ) + " ]";

	System.out.println( output );
    }


    //main method for testing
    public static void main ( String[] args ) {



	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	System.out.println("\nNow testing binSearch on int array...");

	//Declare and initialize array of ints
	int[] iArr = { 2, 4, 6, 8, 6, 42 };
	printArray( iArr );
	System.out.println( "sorted? -- " + isSorted(iArr) );

	int[] iArr2 = { 2, 4, 6, 8, 13, 42 };
	printArray( iArr2 );
	System.out.println( "sorted? -- " + isSorted(iArr2) );

	//search for 6 in array 
	System.out.println( binSearch(iArr,6) );

	//search for 43 in array 
	System.out.println( binSearch(iArr,43) );

    }//end main()

}//end class BinSearch

