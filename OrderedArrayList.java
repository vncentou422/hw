//Vincent Ou
//pd 9
//HW#40
//2013-12-9

/*============================================
   class OrderedArrayList
   Wrapper class for ArrayList.
   Imposes the restriction that stored items 
   must remain sorted in ascending order
   ============================================*/

//ArrayList's implementation is in the java.util package
import java.util.ArrayList;


public class OrderedArrayList {

    // instance of class ArrayList, holding objects of type Comparable 
    // (ie, instances of a class that implements interface Comparable)
    private ArrayList<Comparable> _data;


    // default constructor initializes instance variable _data
    public OrderedArrayList() {
	_data = new ArrayList<Comparable>();
    }


    public String toString() { 
	return _data.toString();
    }


    public Comparable remove( int index ) { 
	return _data.remove( index );
    }


    public int size() { 
	return _data.size();
    }

    
    public Comparable get( int index ) { 
	return _data.get( index );

  
    }


    // addLinear takes as input any comparable object 
    // (i.e., any object of a class implementing interface Comparable)
    // inserts newVal at the appropriate index
    // maintains ascending order of elements
    // uses a linear search to find appropriate index
    public void addLinear(Comparable newVal) { 
	if (_data.size() == 0){
	    _data.add(newVal);
	}
	else{
	    for (int x = 0; x < _data.size(); x++){
		if (newVal.compareTo( _data.get(x)) < 0){
		    _data.add( x , newVal);
		    break;
		}
	    }
	}
    }


    // addBinary takes as input any comparable object 
    // (i.e., any object of a class implementing interface Comparable)
    // inserts newVal at the appropriate index
    // maintains ascending order of elements
    // uses a binary search to find appropriate index
    public void addBinary(Comparable newVal) { 
	if (_data.size() == 0){
	    _data.add(newVal);
	}
	else {
	    int lo = 0;
	    int hi = _data.size() - 1;
	    int m = ( hi + lo )/ 2;
	    while ((_data.get(m).compareTo(newVal) != 0)&&(lo <= hi)){
		if ( _data.get(m).compareTo(newVal) > 0)
		    hi = m - 1;
		else if ( _data.get(m).compareTo(newVal) < 0)
		    lo = m + 1;
		m = (lo + hi )/ 2;
		
	    }
	    if (lo <= hi)
		_data.add(m, newVal);
	    else
		_data.add(lo, newVal);
	
	}
    }



    // main method solely for testing purposes
    public static void main( String[] args ) {

	
	OrderedArrayList Franz = new OrderedArrayList();
	System.out.println(Franz);
	
	// testing linear search
	for( int i = 0; i < 15; i++ )
	    Franz.addLinear( (int)( 50 * Math.random() ) );
	System.out.println( Franz );

	// testing binary search
	Franz = new OrderedArrayList();
	for( int i = 0; i < 15; i++ ) 
	    Franz.addBinary( (int)( 50 * Math.random() ) );
	System.out.println( Franz );
	   
    }

}//end class OrderedArrayList
