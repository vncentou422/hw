//Vincent Ou
//pd 9 
//HW#29
//2013-11-20

//skeleton file for class Binary
// eventually should adhere to specifications of Comparable interface

public class Binaryv2 implements Comparable{

    private int _decNum;
    private String _binNum;

    /*=====================================
      default constructor
      precondition:  
      postcondition: 
      =====================================*/
    public Binaryv2() { 
	_decNum = 0;
	_binNum = "0";
    }


    /*=====================================
      overloaded constructor
      precondition:  n >= 0
      postcondition: 
      =====================================*/
    public Binaryv2( int n ) {
	if (n >= 0){
	    _decNum = n;
	}
	else
	    _decNum = 0;
	_binNum = decToBin(_decNum);

    }

    /*===============================
     accessors
     ================*/
    public int getDecNum(){
	return _decNum;
    }
    public String getBinNum(){
	return _binNum;
    }
    /*=====================================
      String toString() -- returns String representation of this Object
      precondition:  
      postcondition: 
      =====================================*/
    public String toString() { 
	return _binNum;
    }


    /*=====================================
      String decToBin(int) -- converts base-10 input to binary
      precondition:  n >= 0
      postcondition: return String of bits
      eg  decToBin(14) -> "1110"
      =====================================*/
    public static String decToBin( int n ) {
        String retStr = "" + (n%2);
	while (n > 0){
	    retStr = "" + ((n/2) % 2) + retStr;
	    n /= 2;
	}
	return retStr.substring(1);
    }


    /*=====================================
      String decToBinR(int) -- converts base-10 input to binary, recursively
      precondition:  n >= 0
      postcondition: returns String of bits
      eg  decToBin(14) -> "1110"
      =====================================*/
    public static String decToBinR( int n ) { 
	if ((n > 0)){
	    return  decToBinR(n/2) + (n%2);
	}
	else
	    return "";
        
    }

    public boolean equals( Object other ) { 
	return this == other
	    ||
	    (other instanceof Binaryv2
	     && this._decNum == ((Binaryv2)other)._decNum);
    }
    public int compareTo( Object other ) {

	if ( ! (other instanceof Binaryv2) )
	    throw new ClassCastException("\ncompareTo() input not Binary");

	return this._decNum - ((Binaryv2)other)._decNum;
    }
    //main method for testing
    public static void main( String[] args ) {

	System.out.println();
	System.out.println( "Testing ..." );
        
	
	Binaryv2 b1 = new Binaryv2(5);
	
	Binaryv2 b2 = new Binaryv2(5);
	Binaryv2 b3 = b1;
	Binaryv2 b4 = new Binaryv2(7);

	
	System.out.println(b1);
	System.out.println(b2);
	System.out.println(b3);       //false b/c b1, b2 not aliases
	System.out.println(b1 == b2); //true b/c b1, b3 are aliases
	System.out.println(b1 == b3); //false, but should be true
	System.out.println(b1.equals(b2));

	System.out.println( "\n.equals()..." );
	System.out.println( b1.equals(b2) ); //should be true
	System.out.println( b1.equals(b3) ); //should be true
	System.out.println( b3.equals(b1) ); //should be true
	System.out.println( b4.equals(b2) ); //should be false
	System.out.println( b1.equals(b4) ); //should be false

	System.out.println( "\n.compareTo..." );
	System.out.println( b1.compareTo(b2) ); //should be 0
	System.out.println( b1.compareTo(b3) ); //should be 0
	System.out.println( b1.compareTo(b4) ); //should be neg
	System.out.println( b4.compareTo(b1) ); //should be pos
	
    }//end main()

} //end class