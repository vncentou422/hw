//Vincent Ou
//pd 9
//HW#39
//2013-12-5

/*==================================================
  interface ListInt
  Declares methods that will be implemented by any 
  class wishing to adhere to this specification.
  This interface specifies behaviors of a list of ints.
  ==================================================*/

public interface List<T>{

    // Return number of meaningful elements in the list
    int size();
    
    // Append an int to the end. Return true.
    boolean add( T obj);

    // Insert an int at index
    void add( int index, T obj ); 

    // Retrieve the int at index
    T get( int index );

    // Overwrite the int at index
    T set( int index, T obj );

    // Remove the int at index,
    // shifting any elements after it to the left.
    // Return removed value.
    T remove( int index );

}//end interface ListInt
