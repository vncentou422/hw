//Vincent Ou
//pd 9
//HW#49
//2014-1-8

/*====================================
  class Matrix -- models a square matrix

  TASK: Implement methods below, categorize runtime of each. 
        Test in your main method.
  ====================================*/ 

public class Matrix {

    //constant for default matrix size
    private final static int DEFAULT_SIZE = 2;

    private Object[][] _matrix;

    //default constructor intializes a DEFAULT_SIZE*DEFAULT_SIZE matrix
    public Matrix() {
	_matrix = new Object[DEFAULT_SIZE][DEFAULT_SIZE];
    }


    //constructor intializes an a*a matrix
    public Matrix( int a ) {
	_matrix = new Object[a][a];
    }


    //return size of this matrix, where size is 1 dimension
    private int size() {
	return _matrix[0].length;
    }


    //return the item at the specified row & column   
    private Object get( int r, int c ) {
	return _matrix[c-1][r-1];
    }


    //return true if this location is empty, false otherwise
    private boolean isEmpty( int r, int c ) {
	return (_matrix[c - 1][r - 1] == null);
    }


    //overwrite item at specified row and column with newVal
    //return old value
    private Object set( int r, int c, Object newVal ) {
	_matrix[c-1][r-1] = newVal;
	return newVal;
    }


    //return String representation of this matrix
    // (make it look like a matrix)
    public String toString() {
	String retStr = "";
	for (int x = 0; x < _matrix.length; x++){
	    retStr += "|";
	    for (Object[] y : _matrix){
		retStr += y[x] + " ";
	    }
	    retStr = retStr.substring(0, retStr.length() - 1) + "|\n";
	}
	return retStr;
    }
	    

    //override inherited equals method
    //criteria for equality: matrices have identical dimensions,
    // and identical values in each slot
    
    public boolean equals( Object rightSide ) {
        if (!(rightSide instanceof Matrix)){
	    return false;
	}
	else if (((Matrix)rightSide).size() == this.size()){
	    for (int x = 0; x < _matrix.length; x++){
		for(int y = 0; y < _matrix[0].length; y++){
		    Object z = get(x+1,y+1);
		    Object c = ((Matrix)rightSide).get(x+1,y+1);
		    if ( z == null && c == null)
			continue;
		    else if (!(z.equals(c)))
			return false;
		    
		}
	    }
	    return true;
	}
	else{
	    return false;
	}
    }
   

    //return true if target is in this matrix; false otherwise
    public boolean isFound( Object target ) {
	for (Object[] x: _matrix){
	    for (int y = 0; y < x.length; y++){
		if (x[y] == null && target == null){
		    return true;
		}
		else if(x[y].equals(target)){
		    return true;
		}
	    }
	}
	return false;
    }


    //swap two columns of this matrix 
    //(1,1) is top left corner of matrix
    //row values increase going down
    //column value increase L-to-R
    public void swapColumns( int c1, int c2  ) {
	for (int r = 1; r < _matrix.length + 1; r++){
	    set(r,c1,set(r,c2,get(r,c1)));
	}
    }


    //swap two rows of this matrix 
    //(1,1) is top left corner of matrix
    //row values increase going down
    //column value increase L-to-R
    public void swapRows( int r1, int r2  ) {
	for (int c = 1; c < _matrix[0].length + 1; c++){
	    set(r1,c,set(r2,c,get(r1,c)));
	}
    }


    //main method for testing
    public static void main( String[] args ) {
	Matrix x = new Matrix();
	Matrix y = new Matrix(3);
	Matrix z = new Matrix();
	x.set(1,1,1);
	x.set(1,2,2);
	x.set(2,1,3);
	x.set(2,2,4);
	System.out.println(x);
	z.set(1,1,1);
	z.set(1,2,2);
	z.set(2,1,3);
	z.set(2,2,4);
	System.out.println(z);
	System.out.println(x.equals(z));
	System.out.println(x.equals(y));
	System.out.println(x.isFound(1));
	System.out.println(x.isFound(5));
	x.swapColumns(1,2);
	System.out.println(x);
	x.swapRows(1,2);
       	System.out.println(x);
	
    }

}//end class Matrix
