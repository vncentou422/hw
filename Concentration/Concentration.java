/*========================================
class Concentration
Facilitates playing game of Concentration,
aka Memory.
========================================*/

import java.util.ArrayList;
import cs1.Keyboard;

public class Concentration {

    //instance variables
    //storage for 4x4 grid of Tiles. _board.size() == 16	
    private ArrayList<Tile> _board; 

    //count of Tiles with faces visible
    private int _numberFaceUp;  

    //list of Strings used for Tile vals
    private WordList _list;

    private static int _numRows = 4;
    
    public Concentration(){
	_board = new ArrayList<Tile>();
	_numberFaceUp = 0;
	
	ArrayList<String> x = new ArrayList<String>();
	x.add("dog");
	x.add("cat");
	x.add("pig");
	x.add("cow");
	x.add("bee");
	x.add("fox");
	x.add("rat");
	x.add("owl");
	_list = new WordList();
	for( String i : x){
	    _board.add(new Tile(i));
	    _board.add(new Tile(i));
	}
        for (int i = 0;i <1000;i++){
	    int y = (int) (_board.size() * Math.random());
	    int z = (int) (_board.size() * Math.random());
	    _board.set(y,_board.set(z,_board.get(y)));
	}

    }
     
    public void print(){
	String retStr = "";
	for (int x = 0; x < _board.size(); x++){
	    if (x%_numRows == 0)
		retStr += "\n";
	    if (_board.get(x).isFaceUp()){
		retStr += " -" + _board.get(x).getFace() + "-";
	    }
	    else
		retStr += " -" + (x+1) + "-";
	}
	System.out.println(retStr);
	
    }
    public boolean tilesUp(){
                for (int x = 0;x < _board.size();x++){
                        if (!_board.get(x).isFaceUp())
                                return false;
                }
                return true;
        }
   
    public void play(){
	System.out.println("Let's play Concentration");
        print();
	while(!tilesUp()){
	    System.out.println("Choose a tile to flip: ");
	    int x = Keyboard.readInt() - 1;
	    while (x > (_numRows * _numRows - 1) || _board.get(x).isFaceUp()){
		print();
		System.out.println("That tile is already flipped or that is an invalid tile. Please choose a different one");
	        x = Keyboard.readInt() - 1;
	    }
	    _board.get(x).flip();
	    System.out.println(""+ _board.get(x) + "!");
	    print();
	    System.out.println("Choose a second tile to flip: ");
	    int y = Keyboard.readInt() - 1;
	    while (y > (_numRows * _numRows - 1) || _board.get(y).isFaceUp()){
	        print();
		System.out.println("That tile is already flipped or that is an invalid tile. Please choose a different one");
		y = Keyboard.readInt() - 1;
	    }
	    _board.get(y).flip();
	    System.out.println(""+ _board.get(y) + "!");
	    print();
	    if (_board.get(x).equals(_board.get(y))){
		System.out.println("You found a match!");
	    }
	    else{
		System.out.println("No Match!");
		_board.get(x).flip();
		_board.get(y).flip();
		print();
	    }
                        
	}
    }
    //DO NOT MODIFY main()
    public static void main( String[] args ) {
	Concentration game = new Concentration();
	game.play();
    }

}//end class Concentration

