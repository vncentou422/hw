//Vincent Ou
//pd 9
//HW#35
//2013-12-1

/*
Summary of Findings
1) An array of length 100 shows that LinSearch is faster than BinSearch in both scenarios when the target is in the array and when it is not
2) An array of length 2000 shows that LinSearch is a bit slower but overall is faster than BinSearch. BinSearch times apparently have quickened for a target that is in the array
3) An array of length 3 shows times that are drastically increased. LinSearch is still faster than BinSearch but the times have spiked with BinSearch sometimes hitting 20 milliseconds.

Overall, LinSearch appears to be more faster than BinSearch.
*/
public class SearchTrials{
    public static Comparable[] Arr(int length){
	Comparable[] retArr = new Comparable[length];
	int y = 0;
	while (y < length){
	    retArr[y] = y;
	    y++;
	}
	return retArr;
    }
    public static void main(String[] args){
	Comparable[] Arr1 = Arr(200);
	long t = System.currentTimeMillis();
	System.out.println(t); //1385938046848
	System.out.println(Searching.linSearch(Arr1, 100));
	System.out.println(System.currentTimeMillis()- t); //1,2
	System.out.println(Searching.linSearch(Arr1, 250));
	System.out.println(System.currentTimeMillis()- t); //3,4
	System.out.println(BinSearch2.binSearch(Arr1, 100));
	System.out.println(System.currentTimeMillis()- t);//6,7
	System.out.println(BinSearch2.binSearch(Arr1, 250));
	System.out.println(System.currentTimeMillis()- t);//8,8

	Comparable[] Arr2 = Arr(2000);
	long y = System.currentTimeMillis();
	System.out.println(t); //1385938046848
	System.out.println(Searching.linSearch(Arr2, 1000));
	System.out.println(System.currentTimeMillis()- y); //2,3
	System.out.println(Searching.linSearch(Arr2, 2500));
	System.out.println(System.currentTimeMillis()- y); //4,5
	System.out.println(BinSearch2.binSearch(Arr2, 1000));
	System.out.println(System.currentTimeMillis()- y);//5,6
	System.out.println(BinSearch2.binSearch(Arr2, 2500));
	System.out.println(System.currentTimeMillis()- y);//8,8

	Comparable[] Arr3 = Arr(3);
	long x = System.currentTimeMillis();
	System.out.println(t); //1385938046848
	System.out.println(Searching.linSearch(Arr3, 2));
	System.out.println(System.currentTimeMillis()- x); //2/3
	System.out.println(Searching.linSearch(Arr3, 4));
	System.out.println(System.currentTimeMillis()- y); //15/14
	System.out.println(BinSearch2.binSearch(Arr3, 2));
	System.out.println(System.currentTimeMillis()- y);//17/16
	System.out.println(BinSearch2.binSearch(Arr3, 4));
	System.out.println(System.currentTimeMillis()- y);//19/18

	

    }
}
