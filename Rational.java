//Vincent Ou
//pd 9
//HW#28
//2013-11-19
public class Rational implements Comparable{
  
  private int numerator;
  private int denominator;
  public Rational(){
    numerator = 0;
    denominator = 1;
  }
  public Rational(int x, int y){
    if (y == 0){
      numerator = 0;
      denominator = 1;
    }
    else{
      numerator = x;
      denominator = y;
    }
  }
  public String toString(){
    return "The rational number is " + numerator + "/" + denominator;
  }
  public float floatValue(){
    return (float)(numerator/denominator);
  }
  
  
  public void multiply(Rational s){
    numerator = numerator * s.numerator;
    denominator = denominator * s.denominator;
  }
  
  public void divide(Rational s){
    numerator = numerator * s.denominator;
    denominator = denominator * s.numerator;
  }
  public void add(Rational s){
    int x = denominator;
    denominator = denominator * s.denominator;
    numerator = (numerator * s.denominator) + (s.numerator * x);
  }
  public void subtract (Rational s){
    int x = denominator;
    denominator = denominator * s.denominator;
    numerator = (numerator * s.denominator) - (s.numerator * x);
  }    
    public int gcd() {

	int a, b, x;

	if ( numerator > denominator ) {
	    a = numerator;
	    b = denominator;
	}
	else {
	    a = denominator;
	    b = numerator;
	}

	while( a % b != 0 ) {
	    x = a;
	    a = b;
	    b = x % b;
	}

	return b;
    }

  public void reduce(){
    int x = gcd(numerator, denominator);
    numerator = numerator / x;
    denominator = denominator /x;
  }
  public static int gcd( int n, int d ) {

	int a, b, x;

	if ( n > d ) {
	    a = n;
	    b = d;
	}
	else {
	    a = d;
	    b = n;
	}

	while( a % b != 0 ) {
	    x = a;
	    a = b;
	    b = x % b;
	}

	return b;
    }
  /*
  public int compareTo(Rational r){
  	int thisnum, othernum;
  	thisnum = numerator * r.denominator;
  	othernum = denominator * r.numerator;
  	return thisnum - othernum;
  }
  */
  public int compareTo (Object r){
  	int thisnum, othernum;
  	if (r instanceof Rational){
  		thisnum = numerator * ((Rational) r).denominator;
  		othernum = denominator * ((Rational) r).numerator;
  		return thisnum - othernum;
  	}
  	else
  		return -1;
  }
  public boolean equals(Rational r){
  	int thisnum, othernum;
  	thisnum = numerator * r.denominator;
  	othernum = denominator * r.numerator;
  	return thisnum == othernum;
  }
  public static void main(String [] args){
    Rational r = new Rational(2,3);
    Rational s = new Rational(1,2);
    String ha = "ha";
    System.out.println(r);
    System.out.println(s);
    System.out.println(r.floatValue());
    System.out.println(s.floatValue());
    r.multiply(s);
    System.out.println(r);
    r.divide(s);
    System.out.println(r);
    Rational t = new Rational(2,3);
    t.add(s);
    System.out.println(t);
    t.subtract(s);
    System.out.println(t);
    t.reduce();
    System.out.println(t);
    System.out.println(t.compareTo(s));
    Rational z = new Rational(1,2);
    System.out.println(z.compareTo(s));
    System.out.println(z.compareTo(r));
    System.out.println(z.equals(t));
    System.out.println(z.compareTo(ha));
  }
  }
