//Vincent Ou
//pd 9
//HW#43
//2013-12-13

/*===========================================
An AP-style question, for practice:
Write the StatPrinter class below. The StatPrinter Object receives an
ArrayList of nonnegative integers, then builds a frequency ArrayList in which
the index values are the data and the entry at the index is the frequency.
For example, if the received data is    2,3,2,5,1,3    then the frequency 
list would be [0,1,2,2,0,1]. This is read as 0 zeroes, 1 one, 2 twos,
2 threes, 0 fours, 1 five. The size of the frequency list is the equal to
the maximum value of the data.

A capability of the class is to calculate local modes from the frequency 
list. A local mode is a value that is greater than the value at index-1 
and greater than the value at index+1. A local mode is never at the end
points of the list. For example, if the frequency list is [1,2,1,4,2,3,5] 
then the local modes are 2 and 4.

This class is also capable of printing a histogram of the frequencies, using
‘*’s to indicate a frequency amount. To print a histogram, the user specifies
the longest sequence of ‘*’s used and then all other values are printed in 
proportion to this value. For example, if longest bar is 10 and the frequency
list is [1,2,1,4,2,3,5] then the histogram printed looks like this:

0 : **
1 : ****
2 : **
3 : ********
4 : ****
5 : ******
6 : **********

For each method, state run time efficiency using Big O notation.

Tips for Awesome:
* Look over all fxns, think a bit, decide which to tackle first.
	( Simplest?  Prerequisites? . . . )
* Develop 1 fxn at a time, test it, then move to next.
* For coding today, what extra code do you need to get past Mr. Compiler?
===========================================*/

import java.util.ArrayList;

public class StatPrinter {

    // instance variable for frequencies of each integer in input ArrayList
    private ArrayList <Integer> _frequency;


    //*************** QUESTION _2_ **************************
    //precond:  data.size() > 0, each entry b/t 0,100 inclusive
    //postcond: _frequency.size() set to max(data)+1
    //          _frequency.get(i) returns frequency of i in data
    //eg, for data [2,3,2,5,2,3]
    //  _frequency would be [0,0,3,2,0,1]
    public StatPrinter( ArrayList <Integer> data ) { //O(nlogn)
	int max = max(data);
	if (data.size() > 0) {
	    _frequency = new ArrayList<Integer>(max+1);
	    
	    int counter = 0;
	    while (counter < max+1){
		_frequency.add(0);
		counter ++;
	    }
	    for (int x = 0; x < data.size(); x ++){
		int y = _frequency.get(data.get(x));
		_frequency.set(data.get(x), y+1);
	    }
	    
	}
    }


    //*************** QUESTION _1_ **************************
    //precond:  data.size() > 0
    //postcond: returns largest integer in data
    public Integer max( ArrayList <Integer> data ) { //O(n)
	int max = data.get(0);
	for (int x = 0; x < data.size(); x++){
	    if (data.get(x) > max)
		max = data.get(x);
	}
	return max;
    }


    //*************** QUESTION _3_ **************************
    //postcond: returns true if i>0 and i<_frequency.size()-1
    //          and _frequency.get(i-1) < _frequency.get(i)
    //          and _frequency.get(i+1) < _frequency.get(i)
    //          Otherwise, returns false
    //eg, for _frequency [1,2,1,5,5,8,2,4]
    //    2 and 8 are local modes, so
    //    isLocalMode(0) -> false
    //    isLocalMode(1) -> true
    //    isLocalMode(5) -> true
    public boolean isLocalMode( int i ) { //O(1)
	return ((i > 0) && (i < _frequency.size() - 1) && (_frequency.get(i-1) < _frequency.get(i)) && (_frequency.get(i+1) < _frequency.get(i)));
    }


    //*************** QUESTION _4_ **************************
    //postcond: returns list of modes in _frequency
    public ArrayList <Integer> getLocalModes() { //O(n)
        ArrayList<Integer> retArr = new ArrayList<Integer>();
	for (int x = 0; x < _frequency.size(); x++){
	    if (isLocalMode(x))
		retArr.add(_frequency.get(x));
	}
	return retArr;
    }


    //*************** QUESTION 5__ **************************
    //precond:  longestBar > 0
    public void printHistogram( int longestBar ) { //O(nlogn)

	int maxfreq = max(_frequency);
	int ratio = longestBar/maxfreq;
	for (int x = 0; x < _frequency.size(); x++){
	    int y = _frequency.get(x);
	    int count = (ratio * y);
	    String retStr = "" + x + " : ";
	    while (count > 0){
		retStr += "*";
		count--;
	    }
	    System.out.println(retStr);
	
	}

    }
    
    //************** MAIN METHOD ***************************
    public static void main (String [] args){
	ArrayList<Integer> x = new ArrayList<Integer>();
	x.add(2);
	x.add(3);
	x.add(2);
	x.add(5);
	x.add(2);
	x.add(3);
	System.out.println(x);
	StatPrinter printer = new StatPrinter(x);
	System.out.println(printer._frequency);
	printer.printHistogram(5);
	System.out.println(printer.getLocalModes());
    }
}//end class StatPrinter
