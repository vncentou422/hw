 //Vincent Ou
//pd 9
//HW#36
//2013-12-2

/*==================================================
  class SuperArray
  Wrapper class for array. Facilitates resizing, 
  getting and setting element values.
  ==================================================*/

public class SuperArray {

    private int[] _data;
    private int _lastPos;
    private int _size;


    //default constructor
    //initializes 10-item array
    public SuperArray() {
	_size = 10;
	_lastPos = 9;
	_data = new int[_size];

    }
    //output array in [a,b,c] format
    //eg, for int[] a = {1,2,3} ...
    //toString() -> "[1,2,3]"
    public String toString() {	
	String output = "[ "; 

	for( int i : _data )
	    output += i + ", ";

	output = output.substring( 0, output.length()-2 ) + " ]";

	return  output;
    }


    //double capacity of this instance of SuperArray 
    private void expand() { 
	int[] newdata = new int[_data.length * 2];
	
	for (int i = 0; i < _data.length; i++)
	    newdata[i] = _data[i];
	_data = newdata;
    }


    //accessor method -- return value at specified index
    public int get( int index ) {
	return _data[index];
    }


    //mutator method -- set index to newVal, return old value at index
    public int set( int index, int newVal ) {
	_data[index] = newVal;
	return -1; 
    }

    public static void grow(byte[] a, int n){
	byte[] b = new byte[ a.length + n];
	for (int i = 0; i < a.length; i++)
	    b[i] = a[i];
	a = b;
    }
    //main method for testing
    public static void main( String[] args ) {

	
	SuperArray curtis = new SuperArray();
	System.out.println( "Printing empty SuperArray curtis..." );
	System.out.println( curtis );
	for( int i = 0; i < curtis._data.length; i++ ) {
	    curtis.set( i, i * 2 );
	    curtis._size++;
	}

	System.out.println("Printing populated SuperArray curtis...");
	System.out.println(curtis);


    }
    
   
}//end class SuperArray