//Vincent Ou
//pd 9
//HW#42
//2013-12-11

//Team Name: Team 6
//Groupmate: Chesley Tan, Thomas Hlebowicz

/*=====================================
  class CandidatePool -- 

  Each CandidatePool object contains the following
  instance variables that you may need to use:
       
  pool
      An ArrayList of only Candidate objects


  You will need to write the following methods:
   
  getCandidatesForPosition( String pos )
      Returns a candidate-only ArrayList containing all
      the Candidates in pool that have position pos.

  getBestCandidate( String pos )
      Returns the Candidte in pool that matches position
      pos with the highest score.
      If there are no candidates for the given position, 
      returns null.

  removeCandidatesForPosition( String pos )
      Removes all the Candidates in the pool that match
      position pos.
      Returns number of candidates removed.
  =====================================*/

//We usually leave this out because Java does this automatically:
import java.io.*; 
//...but this is not automatically performed (util pkg contains ArrayList):
import java.util.*;


public class CandidatePool {
    
    //=================================
    //DO NOT MODIFY THIS CODE BLOCK
    ArrayList<Candidate> pool;
    
    public CandidatePool() {
	pool = new ArrayList<Candidate>();
    }

    public void addCandidate( Candidate c ) {
	pool.add(c);
    }

    public String toString() {
	return pool.toString();
    }    
    //=================================



    //YOUR WORK BELOW:
    public ArrayList<Candidate> getCandidatesForPosition( String pos ) {
	ArrayList<Candidate> retPool = new ArrayList<Candidate>(); //creates a new ArrayList<Candidate>
	for (int x = 0; x < pool.size(); x++){
	    //searches through the Array and adds it to the new ArrayList if there is a match for position
	    if (pool.get(x).getPosition().equals(pos)){ 
		retPool.add(pool.get(x));
	    }
	}
	return retPool;
    }


    public Candidate getBestCandidate( String pos ) { 
	ArrayList<Candidate> candidatePool = getCandidatesForPosition(pos);
	if (candidatePool.size() == 0){
	    return null;
	}
	Candidate bestCandidate = candidatePool.get(0); //placeholder for best Candidate
	for (int i = 1; i < candidatePool.size(); i ++){
	    //compares to the Candidate scores as it steps through
	    if (candidatePool.get(i).getInterviewScore() > bestCandidate.getInterviewScore()){ 
		bestCandidate = candidatePool.get(i); //sets it to the higher score
	    }
	}
	return bestCandidate;
	    
    }


    public int removeCandidatesForPosition( String pos ) {
	int numRem = 0;
	for (int c = 0; c < pool.size(); c++){ 
	    if (pool.get(c).getPosition().equals(pos)){ //cycles through list to look for position and if there is any remove
		pool.remove(c);
		numRem++;
		c--; //addresses how the size changes every time we remove a candidate with the given position
	    }
	}
	return numRem;

    }


}//end class CandidatePool

    
